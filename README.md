# Brianith's Fallout 4 Configuration

Fallout 4 Script Extender:
    <blockquote>https://f4se.silverlock.org/ </blockquote>

Fallout 4 Configuration Tool for INI Tweaks:
    <blockquote>https://www.nexusmods.com/fallout4/mods/102/ </blockquote>

ENB Preset:
    <blockquote>Subtle ENB https://www.nexusmods.com/fallout4/mods/5885 <br />
    http://enbdev.com/download_mod_fallout4.htm </blockquote>

Mod Organizer:
    <blockquote>Mod Organizer 2:<br />
    https://www.nexusmods.com/skyrimspecialedition/mods/6194 <br />
    <em>(Yes, this is the correct link.)</em></blockquote>

Mods Used:
    <blockquote>Can be found in [modlist.txt](https://gitlab.com/Brianith/fallout4/-/blob/main/modlist) <br />
    [https://www.nexusmods.com/fallout4](https://www.nexusmods.com/fallout4?tab=popular+%28all+time%29) (<strong><em>NSFW</em></strong>)</blockquote>
    <blockquote>Note: TruBy9 is only used because I play on a 21:9 monitor. <br />
          If you want to copy my configuration and don't have 21:9, you don't need it. </blockquote>
